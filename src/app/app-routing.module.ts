import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'bill-history',
    loadChildren: () => import('./bill-history/bill-history.module').then( m => m.BillHistoryPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'package-single',
    loadChildren: () => import('./package-single/package-single.module').then( m => m.PackageSinglePageModule)
  },
  {
    path: 'pay-history',
    loadChildren: () => import('./pay-history/pay-history.module').then( m => m.PayHistoryPageModule)
  },
  {
    path: 'register-complaint',
    loadChildren: () => import('./register-complaint/register-complaint.module').then( m => m.RegisterComplaintPageModule)
  },
  {
    path: 'stb-details',
    loadChildren: () => import('./stb-details/stb-details.module').then( m => m.StbDetailsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
