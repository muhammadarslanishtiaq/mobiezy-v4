import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackageSinglePage } from './package-single.page';

const routes: Routes = [
  {
    path: '',
    component: PackageSinglePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PackageSinglePageRoutingModule {}
