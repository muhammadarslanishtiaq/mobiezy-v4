import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PackageSinglePageRoutingModule } from './package-single-routing.module';

import { PackageSinglePage } from './package-single.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackageSinglePageRoutingModule
  ],
  declarations: [PackageSinglePage]
})
export class PackageSinglePageModule {}
