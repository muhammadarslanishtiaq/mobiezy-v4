import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PackageSinglePage } from './package-single.page';

describe('PackageSinglePage', () => {
  let component: PackageSinglePage;
  let fixture: ComponentFixture<PackageSinglePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageSinglePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PackageSinglePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
