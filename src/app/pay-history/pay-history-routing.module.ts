import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayHistoryPage } from './pay-history.page';

const routes: Routes = [
  {
    path: '',
    component: PayHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayHistoryPageRoutingModule {}
