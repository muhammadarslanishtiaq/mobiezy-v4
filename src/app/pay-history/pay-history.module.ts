import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayHistoryPageRoutingModule } from './pay-history-routing.module';

import { PayHistoryPage } from './pay-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayHistoryPageRoutingModule
  ],
  declarations: [PayHistoryPage]
})
export class PayHistoryPageModule {}
