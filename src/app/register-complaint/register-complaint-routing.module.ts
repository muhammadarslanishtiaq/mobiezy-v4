import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComplaintPage } from './register-complaint.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterComplaintPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterComplaintPageRoutingModule {}
