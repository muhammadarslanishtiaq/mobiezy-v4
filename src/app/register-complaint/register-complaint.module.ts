import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterComplaintPageRoutingModule } from './register-complaint-routing.module';

import { RegisterComplaintPage } from './register-complaint.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterComplaintPageRoutingModule
  ],
  declarations: [RegisterComplaintPage]
})
export class RegisterComplaintPageModule {}
