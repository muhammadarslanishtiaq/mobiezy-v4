import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisterComplaintPage } from './register-complaint.page';

describe('RegisterComplaintPage', () => {
  let component: RegisterComplaintPage;
  let fixture: ComponentFixture<RegisterComplaintPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterComplaintPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterComplaintPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
