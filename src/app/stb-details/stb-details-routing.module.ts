import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StbDetailsPage } from './stb-details.page';

const routes: Routes = [
  {
    path: '',
    component: StbDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StbDetailsPageRoutingModule {}
