import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StbDetailsPageRoutingModule } from './stb-details-routing.module';

import { StbDetailsPage } from './stb-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StbDetailsPageRoutingModule
  ],
  declarations: [StbDetailsPage]
})
export class StbDetailsPageModule {}
